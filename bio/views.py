import random
import time

import math

import os
import collections

from django import template
from django.http import HttpResponseRedirect, HttpResponse
from django.http.request import QueryDict
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template import loader
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, TemplateView, DetailView

from bio.Index import Index
from bio.Lector import LectorGenoma, LectorReads
from bio.forms import SecuenceForm


class response(TemplateView ):
    template_name = 'response.html'

    def editDistance(self, x, y):
        # Create distance matrix
        D = []
        for i in range(len(x) + 1):
            D.append([0] * (len(y) + 1))
        # Initialize first row and column of matrix
        for i in range(len(x) + 1):
            D[i][0] = 0
        for i in range(len(y) + 1):
            D[0][i] = 0
        # Fill in the rest of the matrix
        for i in range(1, len(x) + 1):
            for j in range(1, len(y) + 1):
                distHor = D[i][j - 1] + 1
                distVer = D[i - 1][j] + 1
                if x[i - 1] == y[j - 1]:
                    distDiag = D[i - 1][j - 1]
                else:
                    distDiag = D[i - 1][j - 1] + 1
                D[i][j] = min(distHor, distVer, distDiag)
        # Edit distance is the value in the bottom right corner of the matrix
        minimo = D[len(x)][0]
        for i in range(len(y) + 1):
            if D[len(x)][i] < minimo:
                minimo = D[len(x)][i]
        return minimo

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Llama a la implemetación base para obtener el contexto
        context = super().get_context_data(**kwargs)
        genome= str(self.request.GET.get('genome'))
        reads=str(self.request.GET.get('reads'))

        starting_point = time.time()

        #Lectura de los archivos
        LecGenoma = LectorGenoma()
        genoma_de_referencia = LecGenoma.readGenome(os.path.dirname(os.path.realpath(__file__)) +'/'+genome)
        LecRead = LectorReads()
        reads, calidad = LecRead.readFastq(os.path.dirname(os.path.realpath(__file__)) +'/'+reads)

        # creacion del indice

        index = Index(genoma_de_referencia, 4)

        # Se busca determinar la cobertura de cada base
        cobertura = [[] for i in range(len(genoma_de_referencia))]
        longitud = len(genoma_de_referencia)
        gen_alineado = ['A'] * longitud
        longitud_read = len(reads[0])
        longitud_segmento_1 = int(math.ceil(float(longitud_read) / 3))
        longitud_segmento_2 = int(math.ceil(float(longitud_read) / 3))
        longitud_segmento_3 = len(reads[0]) - longitud_segmento_1 - longitud_segmento_2

        for read in reads:
            # Se toma el primer fragmento del read
            p = read[0:longitud_segmento_1]
            # Se busca en  el indice si existe el fragmento
            indices = index.queryIndex(p, genoma_de_referencia)

            # Se localizan las coincidencias del primer fragmento del read
            for indice in indices:
                # A partir de las coincidencias halladas, se toman los otros dos fragmentos del read y se calcula su Distancia de Edicion con respecto al genoma de referencia
                # Si la suma de estas distancias es menor o igual a 2 se agrega el read a la cobertura
                if indice < longitud - longitud_read and (
                        self.editDistance(read[longitud_segmento_1:longitud_segmento_1 + longitud_segmento_2],
                                          genoma_de_referencia[
                                          indice + longitud_segmento_1:indice + longitud_segmento_1 + longitud_segmento_2]) + self.editDistance(
                    read[longitud_segmento_1 + longitud_segmento_2:longitud_read], genoma_de_referencia[
                                                                                   indice + longitud_segmento_1 + longitud_segmento_2:indice + longitud_read])) < 3:
                    for x in range(0, longitud_read):
                        cobertura[indice + x].append(read[x])

            # Se repite el proceso para el segundo fragmento del read
            p = read[longitud_segmento_1:longitud_segmento_1 + longitud_segmento_2]
            indices = index.queryIndex(p, genoma_de_referencia)
            for indice in indices:
                if indice >= longitud_segmento_1 and indice <= longitud - longitud_segmento_2 - longitud_segmento_3 and (
                        self.editDistance(read[0:longitud_segmento_1],
                                          genoma_de_referencia[
                                          indice - longitud_segmento_1:indice]) + self.editDistance(
                    read[longitud_segmento_1 + longitud_segmento_2:longitud_read], genoma_de_referencia[
                                                                                   indice + longitud_segmento_2:indice + longitud_segmento_2 + longitud_segmento_3])) < 3:
                    for x in range(0, longitud_read):
                        cobertura[indice - longitud_segmento_1 + x].append(read[x])

            # Se vuelve a repetir el proceso para el tercer fragmento del read
            p = read[longitud_segmento_1 + longitud_segmento_2:longitud_read]
            indices = index.queryIndex(p, genoma_de_referencia)
            for indice in indices:
                if indice >= longitud_read and (self.editDistance(read[0:longitud_segmento_1], genoma_de_referencia[
                                                                                               indice - longitud_segmento_1 - longitud_segmento_2:indice - longitud_segmento_2]) + self.editDistance(
                    read[longitud_segmento_1:longitud_segmento_1 + longitud_segmento_2],
                    genoma_de_referencia[indice - longitud_segmento_2:indice])) < 3:
                    for x in range(0, longitud_read):
                        cobertura[indice - longitud_segmento_1 - longitud_segmento_2 + x].append(read[x])

        contador = 0  # contador de errores
        #Generación del archivo FASTA de salida
        path = os.path.dirname(os.path.realpath(__file__)) + '/secuenciado.fa'
        sec_file = open(path, 'w')
        #Header del FASTA
        sec_file.write('>gi|' + str(random.randint(1, 39102786)) + '|')
        for i in range(0, longitud - 1):

            if len(cobertura[i]) != 0:
                gen_alineado[i], count = collections.Counter(cobertura[i]).most_common(1)[
                    0]  # Devuelve la letra mas comun de la cobertura en esa posicion count se utiliza para descartar el numero de repeteciones

            if gen_alineado[i] != genoma_de_referencia[i]:
                contador += 1
            if i % 80 == 0:
                sec_file.write('\n')
                sec_file.write(gen_alineado[i])
            else:
                sec_file.write(gen_alineado[i])

        sec_file.close()
        #Se agregan las variables al contexto para mostrarlas en el template
        context['genalinado'] = gen_alineado
        context['contador'] = str(contador)
        context['precision'] = "{0:.2f}".format((1 - (float(contador)) / longitud) * 100)
        elapsed_time = time.time() - starting_point
        context['duracion'] =  "{0:.2f}".format(elapsed_time)

        return context

""" View que maneja la obtención de los nombres de los archivos a utilizar
    El archivo se debe encontrar en la carpeta de /bio del proyecto
"""
class bio(FormView):

    form_class = SecuenceForm
    template_name = 'Secuenciacion.html'

    def get_success_url(self):
        HttpResponseRedirect(reverse_lazy('response',
                                          kwargs={'genome': self.object, 'reads': self.object}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Add any extra context data needed for form here.
        return context

