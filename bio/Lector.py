

def phred33ToQ(qual):
        return ord(qual) - 33

class LectorReads(object):
    def readFastq(self, filename):
        sequences = []
        qualities = []
        with open(filename) as fh:
            while True:
                fh.readline() # skip name line
                seq = fh.readline().rstrip() # read base sequence
                fh.readline() # skip placeholder line
                qual = fh.readline().rstrip() #base quality line
                if len(seq) == 0:
                    break
                sequences.append(seq)
                qualities.append(qual)
        return sequences, qualities

   

    def createHist(self,qualityStrings):
        # Create a histogram of quality scores
        hist = [0]*50
        for read in qualityStrings:
            for phred in read:
                q = phred33ToQ(phred)
                hist[q] += 1
        return hist


class LectorGenoma(object):
    def readGenome(self, filename):
        genome = ''
        with open(filename, 'r') as f:
            for line in f:
                # ignore header line with genome information
                if not line[0] == '>':
                    genome += line.rstrip()
        return genome
