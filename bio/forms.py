from django import forms

class SecuenceForm(forms.Form):
    genome = forms.FileField(max_length=500)
    reads = forms.FileField(max_length=500)


